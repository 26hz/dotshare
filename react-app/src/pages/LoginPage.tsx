import React from 'react';
import { Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../redux/store';

import { loginThunk, loginFBThunk } from '../redux/auth/thunks';
import ReactFacebookLogin, { ReactFacebookLoginInfo } from 'react-facebook-login';

interface IFormInput {
    username: string;
    password: string;
}

const LoginPage: React.FC = () => {
    const dispatch = useDispatch();
    const errMessage = useSelector((state: IRootState) => state.auth.message);
    const isProcessing = useSelector((state: IRootState) => state.auth.isProcessing);

    const { register, handleSubmit } = useForm<IFormInput>();

    const onSubmit = (data: IFormInput) => {
        console.log(data);
        if (!isProcessing) {
            dispatch(loginThunk(data.username, data.password));
        }
    };

    const fBOnCLick = () => {
        return null;
    };

    const fBCallback = (userInfo: ReactFacebookLoginInfo & { accessToken: string }) => {
        console.log(userInfo);
        if (userInfo.accessToken) {
            dispatch(loginFBThunk(userInfo.accessToken));
        }
    };

    return (
        <div>
            {console.log('login page...')}
            <h1>This is Login Page</h1>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <FormGroup>
                    <Label for="username">Username</Label>
                    <Input type="text" name="username" placeholder="Type in Username" innerRef={register} />
                </FormGroup>
                <FormGroup>
                    <Label for="password">Password</Label>
                    <Input type="password" name="password" placeholder="Type in Password" innerRef={register} />
                </FormGroup>
                {errMessage ? <Alert color="danger">{errMessage}</Alert> : ''}
                <Input type="submit" value="Submit" />
                <hr />
                <FormGroup>
                    <div className="fb-button">
                        <ReactFacebookLogin
                            appId={process.env.REACT_APP_FACEBOOK_APP_ID || ''}
                            autoLoad={false}
                            fields="name,email,picture"
                            onClick={fBOnCLick}
                            callback={fBCallback}
                        />
                    </div>
                </FormGroup>
            </Form>
        </div>
    );
};

export default LoginPage;
