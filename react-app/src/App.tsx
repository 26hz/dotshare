import React, { useEffect } from 'react';
import './App.css';

import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from './redux/store';
import { Switch, Route } from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';

import { restoreLoginThunk } from './redux/auth/thunks';

import logo from "./assets/img/dot-share-logo.png";

import arrowDownIcon from "./assets/icon/arrow-down.svg";
import showIcon from "./assets/icon/show.svg";
import hideIcon from "./assets/icon/hide.svg";
import checkerIcon from "./assets/icon/checker.svg";
import solidIcon from "./assets/icon/solid.svg";
import textureIcon from "./assets/icon/texture.svg";
import litIcon from "./assets/icon/lit.svg";
import unlitIcon from "./assets/icon/unlit.svg";

function App() {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
    console.log(isAuthenticated);

    useEffect(() => {
        if (isAuthenticated === null) {
            // restore login
            dispatch(restoreLoginThunk());
        }
    }, [dispatch, isAuthenticated]);

    return (
        <div className="App">
            {/* <h1>This is React App</h1> */}
            {/* {isAuthenticated === null && <h1>restoring login...</h1>}
            {isAuthenticated !== null && (
                <Switch>
                    <PrivateRoute path="/" exact={true} component={HomePage} />
                    <Route path="/login" exact={true} component={LoginPage} />
                </Switch>
            )} */}

						<header>
							<div>
								<img className="dotShare-logo" src={logo} />
								<div className="text-dark-red text-bold text-24">Share</div>
							</div>
							<div>
								<div className="text-24 text-gray text-thin mr-2">Projects /</div>
								<div className="text-24 text-dark-gray text-thin mr-2">Furniture</div>
								<img className="arrow-icons" src={arrowDownIcon} />
							</div>
							<div>
								<div className="users-icon">
									<div className="user-icon">
										L
									</div>
									<div className="user-icon">
										R
									</div>
									<div className="user-icon">
										K
									</div>
									<div className="user-icon">
										E
									</div>
								</div>
								<div className="share-button">
									Share
								</div>
								<div className="user-account">
									<div className="user-icon">L</div>
									<div className="text-16 text-bold">Lucas</div>
								</div>
							</div>
						</header>

						<div className="content">
							<div className="inspector-container">
								<div className="mb-32 text-bold text-dark-gray">Model Inspector</div>
								<div>
									<div className="text-16 text-bold mb-16">
										Wireframe
									</div>
									<div className="mb-16 clickable inspector-buttons">
										<img className="inspector-button-icons mr-12" src={showIcon} />
										<div>Show wireframe</div>
									</div>
									<div className="color-buttons mb-24">
										<div className="mr-12 gray"></div>
										<div className="mr-12 green"></div>
										<div className="mr-12 yellow"></div>
										<div className="mr-12 purple"></div>
										<div className="mr-12 tosca"></div>
										<div className="mr-12 red"></div>
									</div>
									<div className="clickable inspector-buttons mb-32 active">
										<img className="inspector-button-icons mr-12" src={hideIcon} />
										<div>Hide wireframe</div>
									</div>
								</div>
								<div>
									<div className="text-16 text-bold mb-16">
										Material
									</div>
									<div className="mb-16 clickable inspector-buttons">
										<img className="inspector-button-icons mr-12" src={checkerIcon} />
										<div>Show checker</div>
									</div>
									<div className="mb-16 clickable inspector-buttons">
										<img className="inspector-button-icons mr-12" src={solidIcon} />
										<div>Show solid color</div>
									</div>
									<div className="mb-16 clickable inspector-buttons active">
										<img className="inspector-button-icons mr-12" src={textureIcon} />
										<div>Show texture</div>
									</div>
								</div>
								<div>
									<div className="text-16 text-bold mb-16">
										Lighting
									</div>
									<div className="mb-16 clickable inspector-buttons">
										<img className="inspector-button-icons mr-12" src={litIcon} />
										<div>Lit</div>
									</div>
									<div className="clickable inspector-buttons">
										<img className="inspector-button-icons mr-12" src={unlitIcon} />
										<div>Unlit</div>
									</div>
								</div>
							</div>
							<div className="project-info">
								<p>Sup?</p>
								<p>Wish that other guy was the same height as me</p>
							</div>
						</div>

						<div className="centering-bottom-buttons mt-16 mb-16">
							<div className="clickable">
								<div className="ml-32 mr-24">View</div>
								<div className="edit-button active">Edit</div>
							</div>
						</div>

						<footer>

						</footer>
        </div>
    );
}

export default App;
