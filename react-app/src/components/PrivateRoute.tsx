import React from 'react';
import { RouteProps, Redirect, Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { IRootState } from '../redux/store';

// const props = { name: 'jason', age: 18 };
// const { name, ...others } = props;

const PrivateRoute: React.FC<RouteProps> = ({ component, ...rest }) => {
    console.log('PrivateRoute');
    console.log(rest);
    const isAuthenticated = useSelector((state: IRootState) => state.auth.isAuthenticated);
    const Component = component as any;
    if (Component === null) {
        return null;
    }

    let render: (props: any) => JSX.Element;
    if (isAuthenticated) {
        // login jor
        console.log('isAuthenticated');
        render = (props: any) => <Component {...props} />;
    } else {
        console.log('is not isAuthenticated');
        render = (props: any) => (
            <Redirect
                to={{
                    pathname: '/login',
                    state: { from: props.location },
                }}
            />
        );
    }
    return <Route {...rest} render={render} />;
};

export default PrivateRoute;
