// step 1: interface
export interface IAuthState {
    isAuthenticated: boolean | null;
    isProcessing: boolean;
    message: string;
}

// step 2: initState
export const initAuthState: IAuthState = {
    isAuthenticated: null,
    isProcessing: false,
    message: '',
};
