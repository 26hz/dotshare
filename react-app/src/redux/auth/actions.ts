export const LOGIN_PROCESSING = '@@auth/LOGIN_PROCESSING';
export const LOGIN_SUCCESS = '@@auth/LOGIN_SUCCESS';
export const LOGIN_FAIL = '@@auth/LOGIN_FAIL';

interface ILoginProcessing {
    type: typeof LOGIN_PROCESSING;
}

interface ILoginSuccess {
    type: typeof LOGIN_SUCCESS;
}

interface ILoginFail {
    type: typeof LOGIN_FAIL;
    message: string;
}

// action creator
export const loginProcessing = (): ILoginProcessing => {
    return {
        type: LOGIN_PROCESSING,
    };
};

export const loginSuccess = (): ILoginSuccess => {
    return {
        type: LOGIN_SUCCESS,
    };
};

export const loginFail = (message: string): ILoginFail => {
    return {
        type: LOGIN_FAIL,
        message,
    };
};

export type IAuthActions = ILoginProcessing | ILoginSuccess | ILoginFail;
