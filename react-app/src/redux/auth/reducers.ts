import { IAuthState, initAuthState } from './state';
import { IAuthActions } from './actions';
import { LOGIN_PROCESSING, LOGIN_SUCCESS, LOGIN_FAIL } from './actions';

export const authReducers = (state: IAuthState = initAuthState, action: IAuthActions): IAuthState => {
    switch (action.type) {
        case LOGIN_PROCESSING:
            return {
                ...state,
                isAuthenticated: false,
                message: '',
                isProcessing: true,
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isProcessing: false,
                isAuthenticated: true,
            };
        case LOGIN_FAIL:
            return {
                ...state,
                isProcessing: false,
                isAuthenticated: false,
                message: action.message,
            };
        default:
            return state;
    }
};
