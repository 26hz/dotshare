import { Dispatch } from 'redux';
import { loginProcessing, loginSuccess, loginFail } from './actions';
import { push } from 'connected-react-router';
import { IRootState } from '../store';

const { REACT_APP_API_SERVER } = process.env;

export const loginFBThunk = (accessToken: string) => {
    return async (dispatch: Dispatch) => {
        // try {} catch() {}
        dispatch(loginProcessing());
        const res = await fetch(`${REACT_APP_API_SERVER}/user/login/facebook`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ accessToken }),
        });
        const data = await res.json();

        if (res.status === 200) {
            localStorage.setItem('token', data.token);
            dispatch(loginSuccess());
            dispatch(push('/'));
        } else {
            dispatch(loginFail(data.message));
        }
    };
};

export const restoreLoginThunk = () => {
    return async (dispatch: Dispatch, getState: () => IRootState) => {
        console.log('restoreLoginThunk()');
        // try {} catch (err) {}
        const token = localStorage.getItem('token');
        if (token) {
            const res = await fetch(`${REACT_APP_API_SERVER}/user/info`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            if (res.status === 200) {
                dispatch(loginSuccess());
                // dispatch(push('/'));
                dispatch(push(getState().router.location.pathname));
            } else {
                dispatch(loginFail(''));
            }
        } else {
            // Login Fail
            const action = loginFail('');
            dispatch(action);
        }
    };
};

export const loginThunk = (username: string, password: string) => {
    return async (dispatch: Dispatch) => {
        // try {} catch (err) {}
        console.log('loginThunk()');
        console.log(REACT_APP_API_SERVER);

        dispatch(loginProcessing());

        const res = await fetch(`${REACT_APP_API_SERVER}/user/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ username, password }),
        });
        const data = await res.json();

        if (res.status === 200) {
            // SUCCESS !!!
            // store the token into local storage
            localStorage.setItem('token', data.token);
            // call action creator to create an action
            const action = loginSuccess();
            // dispatch action
            dispatch(action);
            dispatch(push('/'));
        } else {
            // FAIL !!!
            const action = loginFail(data.message);
            dispatch(action);
        }
    };
};
