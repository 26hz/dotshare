import express from "express";
import jwtSimple from "jwt-simple";
import jwt from "./jwt";
import { Bearer } from "permit";
import { UserService } from "./services/UserService";

const permit = new Bearer({
    query: "access_token",
});

export function createIsLoggedIn(userService: UserService) {
    return async function (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        try {
            const token = permit.check(req);
            console.log("get token from permit");
            console.log(token)
            if (!token) {
                return res.status(401).json({ msg: "Permission Denied" });
            }
            const payload: { id: number } = jwtSimple.decode(
                token,
                jwt.jwtSecret
            );
            console.log("get payload from token")
            console.log(payload);
            const user = await userService.getUserByID(payload.id);
            if (!user) {
                return res.status(401).json({ msg: "Permission Denied" });
            }
            // user: { id, username, password }
            // const {password, ...others} = user;
            // console.log(others) -> { id, username }
            // { ...others } -> {} - > { id, username }
            const { password, ...others } = user;
            req.user = { ...others };
            return next();
        } catch (err) {
            return res.status(401).json({ msg: "Permission Denied" });
        }
    };
}
