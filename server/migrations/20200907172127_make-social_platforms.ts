import * as Knex from 'knex';

const socialPlatformsTable = 'social_platforms';
const usersTable = 'users';

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(socialPlatformsTable, (table) => {
        table.increments();
        table.integer('user_id').unsigned().notNullable();
        table.foreign('user_id').references(`${usersTable}.id`);
        table.string('platform_type').notNullable().comment('facebook, google');
        table.string('platform_id').notNullable();
        table.timestamps(false, true);
        table.unique(['platform_type', 'platform_id']);
    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(socialPlatformsTable);
}
