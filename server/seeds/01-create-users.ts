import * as Knex from 'knex';

const usersTable = 'users';

import { hashPassword } from '../hash';

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(usersTable).del();

    // Inserts seed entries
    const hashedPassword = await hashPassword('1234');
    await knex(usersTable).insert({ username: 'jason', password: hashedPassword });
    await knex(usersTable).insert({ username: 'peter', password: hashedPassword });
}
