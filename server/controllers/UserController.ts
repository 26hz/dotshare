import { UserService } from '../services/UserService';
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import { Request, Response } from 'express';
import { checkPassword, hashPassword } from '../hash';
import fetch from 'node-fetch';

export class UserController {
    constructor(private userService: UserService) {}

    login = async (req: Request, res: Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                res.status(401).json({ message: 'Missing Username/Password' });
                return;
            }
            const { username, password } = req.body;
            const user = await this.userService.getUserByUsername(username);
            if (!user || !(await checkPassword(password, user.password))) {
                res.status(401).json({ message: 'Wrong Username/Password' });
                return;
            }
            const payload = {
                id: user.id,
                // username: user.username,
            };
            console.log('prepare to generate token');
            console.log(payload);
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            console.log('after generating token');
            console.log(token);
            res.json({
                token: token,
            });
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loginFacebook = async (req: Request, res: Response) => {
        try {
            if (!req.body.accessToken) {
                res.status(401).json({ msg: 'Wrong Access Token!' });
                return;
            }
            const { accessToken } = req.body;
            const fetchResponse = await fetch(
                `https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`
            );
            const result = await fetchResponse.json();
            if (result.error) {
                res.status(401).json({ msg: 'Wrong Access Token!' });
                return;
            }
            console.log(result);
            let payload: { id: number };
            const user = await this.userService.getUserBySocialID('facebook', result.id);
            if (!user) {
                const password = await hashPassword('jasonishandsome');
                const userID = await this.userService.createUserWithSocial(
                    result.email,
                    password,
                    'facebook',
                    result.id
                );
                payload = { id: userID };
            } else {
                // user found
                payload = { id: user.id };
            }
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({
                token: token,
            });
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    getInfo = async (req: Request, res: Response) => {
        try {
            const user = req.user;
            res.status(200).json({
                user: {
                    username: user?.username,
                },
            });
        } catch (e) {
            console.log(e.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };
}
