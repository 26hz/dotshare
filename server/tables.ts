export const tables = Object.freeze({
    USERS: 'users',
    SOCIAL_PLATFORMS: 'social_platforms',
});
