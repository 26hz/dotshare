import express from 'express';
import { userController } from '../main';
import { isLoggedIn } from '../main';

export const userRoutes = express.Router();

userRoutes.post('/login/facebook', userController.loginFacebook);
userRoutes.post('/login', userController.login);
userRoutes.get('/info', isLoggedIn, userController.getInfo);
