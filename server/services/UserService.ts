import Knex from 'knex';
import { IUser } from './models';
import { tables } from '../tables';

export class UserService {
    constructor(private knex: Knex) {}

    createUserWithSocial = async (username: string, password: string, socialType: string, socialID: string) => {
        const trx = await this.knex.transaction();
        try {
            const [id] = await trx(tables.USERS)
                .insert({
                    username,
                    password,
                })
                .returning('id');
            await trx(tables.SOCIAL_PLATFORMS).insert({
                user_id: id,
                platform_type: socialType,
                platform_id: socialID,
            });
            await trx.commit();
            return id as number;
        } catch (err) {
            await trx.rollback();
            throw err;
        }
    };

    getUserBySocialID = async (socialType: string, socialID: string) => {
        const user: IUser = await this.knex(tables.USERS)
            .select(`${tables.USERS}.*`)
            .innerJoin(tables.SOCIAL_PLATFORMS, `${tables.USERS}.id`, '=', `${tables.SOCIAL_PLATFORMS}.user_id`)
            .where(`${tables.SOCIAL_PLATFORMS}.platform_type`, socialType)
            .andWhere(`${tables.SOCIAL_PLATFORMS}.platform_id`, socialID)
            .first();
        // console.log(userQuery.toSQL());
        return user;
    };

    getUserByUsername = async (username: string) => {
        const user = await this.knex<IUser>(tables.USERS).where('username', username).first();
        return user;
    };

    getUserByID = async (id: number) => {
        const user = await this.knex<IUser>(tables.USERS).where('id', id).first();
        return user;
    };
}
